import albumentations as A
from albumentations.pytorch import ToTensorV2
import numpy as np
import torch

train_transform = A.Compose([
    # reszie
    A.Resize(256, 256),
    # 
    A.OneOf([
        # A.VerticalFlip(p=0.5),
        # A.HorizontalFlip(p=0.5),
        # A.RandomRotate90(p=0.5),
        # A.Transpose(p=0.5)
        #https://blog.csdn.net/qq_27039891/article/details/100795846
        A.VerticalFlip(p=0.5), #水平翻转
        A.HorizontalFlip(p=0.5), #垂直翻转
        A.Flip(p=0.5), #水平和垂直翻转
        A.CLAHE(p=0.5), # 对比度受限直方图均衡
        A.RGBShift(p=0.5),#参数：随机平移R、G、B通道值。
        # A.RandomCrop(200,200,p=0.5), #随机裁剪
        A.HueSaturationValue(p=0.5), #色调饱和度值
        # A.RandomScale(p=0.5), #随机缩放图像大小。
        A.RandomBrightnessContrast(p=0.5), #随机亮度对比度
        A.ISONoise(p=0.5),#施加摄像头传感器噪音
        A.RandomGamma(p=0.5),#随机伽马变换。
        A.Blur(p=0.5), #图像均值平滑滤波。
        A.MedianBlur(p=0.5), #图像中值滤波。
        A.Cutout(p=0.5), #在图像中生成正方形区域。
        A.GaussianBlur(p=0.5), # 高斯模糊
        A.MotionBlur(p=0.5), # 给图像加上运动模糊。运动模糊是景物图象中的移动效果。它比较明显地出现在长时间暴光或场景内的物体快速移动的情形里。
        A.JpegCompression(p=0.5),
        A.RandomRotate90(p=0.5), # 随机旋转 90°
        # A.Rotate(p=0.5),#随机旋转图片
        A.OpticalDistortion(p=0.5), #对图像进行光学畸变。
        A.GridDistortion(p=0.5),#对图像进行网格失真。
        #A.ElasticTransform(p=0.5),# 随机对图像进行弹性变换。
        A.Transpose(p=0.5) # 转置 将图像行和列互换
    ]),
    A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
    ToTensorV2(),
])

val_transform = A.Compose([
    A.Resize(256, 256),
    A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
    ToTensorV2(),
])

def mixup_data(x, y, alpha=1.0, device="cpu"):
    '''Compute the mixup data. Return mixed inputs, pairs of targets, and lambda'''
    if alpha > 0.:
        lam = np.random.beta(alpha, alpha)
    else:
        lam = 1.
    batch_size = x.size()[0]
    if str(device) != "cpu":
        index = torch.randperm(batch_size).to(device)
    else:
        index = torch.randperm(batch_size)

    mixed_x = lam * x + (1 - lam) * x[index, :]
    y_a, y_b = y, y[index]
    return mixed_x, y_a, y_b, lam


def mixup_criterion(y_a, y_b, lam):
    return lambda criterion, pred: lam * criterion(pred, y_a) + (1 - lam) * criterion(pred, y_b)