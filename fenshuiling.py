'''
Author      : now more
Connect     : lin.honghui@qq.com
LastEditors: Please set LastEditors
Description : 
LastEditTime: 2020-11-27 03:42:46
'''
import os
import threading
import cv2 as cv
import numpy as np
from scipy import ndimage as ndi
from skimage.morphology import remove_small_holes, remove_small_objects, watershed
from argparse import ArgumentParser
from PIL import Image
import matplotlib.pyplot as plt
from tqdm import tqdm

Image.MAX_IMAGE_PIXELS = 10000000000000000


def to_categorical(y, num_classes=None, dtype='float32'):
    """Converts a class vector (integers) to binary class matrix.

    E.g. for use with categorical_crossentropy.

    # Arguments
        y: class vector to be converted into a matrix
            (integers from 0 to num_classes).
        num_classes: total number of classes.
        dtype: The data type expected by the input, as a string
            (`float32`, `float64`, `int32`...)

    # Returns
        A binary matrix representation of the input. The classes axis
        is placed last.
    """
    y = np.array(y, dtype='int')
    input_shape = y.shape
    if input_shape and input_shape[-1] == 1 and len(input_shape) > 1:
        input_shape = tuple(input_shape[:-1])
    y = y.ravel()
    if not num_classes:
        num_classes = np.max(y) + 1
    n = y.shape[0]
    categorical = np.zeros((n, num_classes), dtype=dtype)
    categorical[np.arange(n), y] = 1
    output_shape = input_shape + (num_classes,)
    categorical = np.reshape(categorical, output_shape)
    return categorical, num_classes


class MyThread(threading.Thread):

    def __init__(self, func, args=()):
        super(MyThread, self).__init__()
        self.func = func
        self.args = args

    def run(self):
        self.result = self.func(*self.args)

    def get_result(self):
        try:
            return self.result  # 如果子线程不使用join方法，此处可能会报没有self.result的错误
        except Exception:
            return None


def label_resize_vis(label, img=None, alpha=0.5):
    '''
    :param label:原始标签 
    :param img: 原始图像
    :param alpha: 透明度
    :return: 可视化标签
    '''
    # label = cv.resize(label.copy(),None,fx=0.1,fy=0.1)
    r = np.where(label == 1, 255, 0)
    g = np.where(label == 2, 255, 0)
    b = np.where(label == 3, 255, 0)
    anno_vis = np.dstack((b, g, r)).astype(np.uint8)
    # 黄色分量(红255, 绿255, 蓝0)
    for npmax_index in range(np.max(label) - 3):
        anno_vis[:, :, 0] = anno_vis[:, :, 0] + np.where(label == 3 + 1 + npmax_index, 255, 0)
        anno_vis[:, :, 1] = anno_vis[:, :, 1] + np.where(label == 3 + 1 + npmax_index, 255, 0)
        anno_vis[:, :, 2] = anno_vis[:, :, 2] + np.where(label == 3 + 1 + npmax_index, 255, 0)
    if img is None:
        return anno_vis
    else:
        overlapping = cv.addWeighted(img, alpha, anno_vis, 1 - alpha, 0)
        return overlapping


def remove_small_objects_and_holes(class_type, label, min_size, area_threshold, in_place=True):
    # kernel = cv.getStructuringElement(cv.MORPH_RECT,(500,500))
    # label = cv.dilate(label,kernel)
    # kernel = cv.getStructuringElement(cv.MORPH_RECT,(10,10))
    # label = cv.erode(label,kernel)
    mask_img = remove_small_objects(label == 1, min_size=min_size, connectivity=1, in_place=in_place)
    mask_img = remove_small_holes(mask_img == 1, area_threshold=area_threshold, connectivity=1, in_place=in_place)
    labeled_array = my_watershed(mask_img, mask_img, label)
    return labeled_array


def my_watershed(what, mask1, mask2):
    # markers = ndi.label(mask2, output=np.uint32)[0]
    # big_seeds = watershed(what, markers, mask=mask1, watershed_line=False)
    # m2 = mask1 - (big_seeds > 0)
    # mask2 = mask2 | m2

    markers = ndi.label(mask2, output=np.uint32)[0]
    labels = watershed(what, markers, mask=mask1, watershed_line=True)
    # labels = watershed(what, markers, mask=mask1, watershed_line=False)
    return labels

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-source_dir", type=str,
                        default=r"C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\data\suichang_round1_test_partA_210120",
                        help="传入image_n_predict所在路径")
    parser.add_argument("-mask_dir", type=str, default=r"C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\data\finally", help="传入image_n_predict所在路径")
    parser.add_argument("-min_size", type=int, default=1)  # 填充孔洞
    parser.add_argument("-threshold", type=int, default=1)  # 去除小连接区域
    arg = parser.parse_args()
    source_dir = arg.source_dir
    mask_dir = arg.mask_dir
    min_size = arg.min_size
    threshold = arg.threshold

    for mask_name in tqdm(os.listdir(mask_dir)):
        source_image = cv.imread(os.path.join(source_dir, mask_name.replace(".png", ".tif")))
        mask = np.asarray(Image.open(os.path.join(mask_dir, mask_name)))

        # mask_post = label_resize_vis(mask, source_image)
        # cv.imwrite(os.path.join(r"C:\Users\zengxh\Desktop\viresulto", mask_name), mask_post)

        mask = mask - 1  # 如果分类是从1开始这里需要减去1
        mask_tocate, num_classes = to_categorical(mask, num_classes=None, dtype='uint8')

        threading_list = []
        for i in range(num_classes):
            t = MyThread(remove_small_objects_and_holes, args=(i, mask_tocate[:, :, i], min_size, threshold, True))
            threading_list.append(t)
            t.start()

        # 等待所有线程运行完毕
        result = []
        for t in threading_list:
            t.join()
            result.append(t.get_result()[:, :, None])

        label = np.concatenate(result, axis=2)

        label = np.argmax(label, axis=2).astype(np.int)
        label = label + 1  # 如果分类是从1开始，由于上面减去了1所以这里加1

        labelsjson = {
            1: "耕地",
            2: "林地",
            3: "草地",
            4: "道路",
            5: "城镇建设用地",
            6: "农村建设用地",
            7: "工业用地",
            8: "构筑物",
            9: "水域",
            10: "裸地"
        }
        mask = mask + 1  # 如果分类是从1开始，由于上面减去了1所以这里加1
        # 以下4类不处理
        # label[np.where(mask == 1)] = 1
        # label[np.where(mask == 3)] = 3
        # label[np.where(mask == 4)] = 4
        # label[np.where(mask == 8)] = 8
        cv.imwrite(os.path.join(r"C:\Users\zengxh\Desktop\results", mask_name), label)

        # mask_post = label_resize_vis(label, source_image)
        # cv.imwrite(os.path.join(r"C:\Users\zengxh\Desktop\viresult",mask_name), mask_post)
