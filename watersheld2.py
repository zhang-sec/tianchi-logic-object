import copy
import os

from matplotlib import pyplot as plt
from cv2 import cv2 as cv
import numpy as np

from test_transforms import draw


def watearshel_demo():
    print(img.shape)
    # blured = cv.pyrMeanShiftFiltering(img, 10, 80)
    # 操作步骤  gray/binary image
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)  # 观察灰度处理后的图像好不好，不好就进行滤波
    ret, binary = cv.threshold(gray, 0, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)
    cv.imshow("binary_image", binary)

    # morphology operation
    kernel = cv.getStructuringElement(cv.MORPH_RECT, (1, 1))  # 结构元素
    mb = cv.morphologyEx(binary, cv.MORPH_OPEN, kernel, iterations=1)  # 进行2次开操作
    sure_bg = cv.dilate(mb, kernel, iterations=1)  # 进行3次膨胀
    cv.imshow("mor", sure_bg)

    # distace transform
    # 对mb进行距离变换
    dist = cv.distanceTransform(mb, cv.DIST_L2, 3)
    dist_output = cv.normalize(dist, 0, 255.0, cv.NORM_MINMAX)
    cv.imshow("distanc_transform", dist_output )

    ret, surface = cv.threshold(dist, dist.max() * 0.6, 255, cv.THRESH_BINARY)
    cv.imshow("surface", surface)

    surface_fg = np.uint8(dist_output)  # 转成int
    unknow = cv.subtract(sure_bg, surface_fg)
    ret, markers = cv.connectedComponents(surface_fg)
    print(ret)

    # cv.imshow("markers", markers)
    # watershed transform
    markers = markers + 1
    markers[unknow == 255] = 0
    markers = cv.watershed(img, markers=markers)
    # img[markers == -1] = [0, 0, 255]  # 红色
    # cv.imshow('result', img)

    markers = markers+1
    markers_o = cv.imread(os.path.join(r"C:\Users\zengxh\Desktop\results", filename + ".png"), cv.IMREAD_GRAYSCALE)
    draw(img, markers_o)
    maskset = set(markers_o.flatten().tolist())
    masktmp = copy.deepcopy(markers_o)
    for classnum in maskset:
        masktmp2 = copy.deepcopy(markers_o)
        # masktmp2[np.where(masktmp2 == classnum)] = 255
        masktmp2[np.where(masktmp2 != classnum)] = 0
        # cv2.imshow('masktmp2', masktmp2)
        dilate = cv.dilate(masktmp2, kernel, iterations=1)
        dilatepos = np.where(dilate == classnum)
        # cv2.imshow('dilate', dilate)

        maskpos = np.where(markers_o == classnum)
        counts = np.bincount(markers[maskpos])
        label_classnum = np.argmax(counts)  # 众数为classnum对应label中的分类

        labelttmp = copy.deepcopy(markers)
        label_classnum_pos = np.where(labelttmp == label_classnum)  # labelclassnum的位置
        labelttmp[label_classnum_pos] = classnum

        labelttmp[dilatepos] = labelttmp[dilatepos] + 11

        label_classnum_pos2 = np.where(labelttmp == (classnum + 11))

        masktmp[label_classnum_pos2] = classnum

    draw(img, masktmp)

    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(6, 6))
    axes = axes.ravel()
    ax0, ax1, ax2, ax3 = axes

    cmap = plt.cm.get_cmap("Spectral")
    # ax1.imshow(markers_o, cmap=cmap, interpolation='nearest')
    # ax1.set_title("markers_o")
    ax2.imshow(markers_o, cmap=cmap, interpolation='nearest')
    ax2.set_title("markers_o")
    ax3.imshow(markers, cmap=cmap, interpolation='nearest')
    ax3.set_title("markers")
    ax1.imshow(masktmp, cmap=cmap, interpolation='nearest')
    ax1.set_title("masktmp")
    for ax in axes:
        ax.axis('off')

    fig.tight_layout()
    plt.show()
    # cv.imwrite('C:\\pictures\\Test paper\\6.jpg',img)


if __name__ == "__main__":
    # filepath = "C:\\pictures\\Test paper\\unqualified\\2019_01_09__16_09_03_452_A.jpg"
    # filepath = "C:\\pictures\\others\\measure2.jpg"
    # img = cv.imread(filepath)  # blue green red
    filename = "000001"
    img = cv.imread(os.path.join(
        r"C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\data\suichang_round1_test_partA_210120",
        filename + ".tif"), cv.IMREAD_COLOR)
    # img = cv.cvtColor(image_o, cv.COLOR_BGR2RGB)
    cv.imshow("inputimage", img)

    watearshel_demo()

    cv.waitKey(0)
    cv.destroyAllWindows()