'''
Author      : now more
Connect     : lin.honghui@qq.com
LastEditors: Please set LastEditors
Description :
LastEditTime: 2020-11-27 03:42:46
'''
import copy
import os
import threading
import cv2 as cv
import numpy as np
from skimage.morphology import remove_small_holes, remove_small_objects
from argparse import ArgumentParser
from PIL import Image
import matplotlib.pyplot as plt
from tqdm import tqdm
import torch

def use_mask2(mask1,mask2,classnum):
    mask1tmp = copy.deepcopy(mask1)
    if classnum in mask2:
        mask1tmp[np.where(mask2 == classnum)] = classnum
    return mask1tmp

def to_categorical(y, num_classes=None, dtype='float32'):
    """Converts a class vector (integers) to binary class matrix.

    E.g. for use with categorical_crossentropy.

    # Arguments
        y: class vector to be converted into a matrix
            (integers from 0 to num_classes).
        num_classes: total number of classes.
        dtype: The data type expected by the input, as a string
            (`float32`, `float64`, `int32`...)

    # Returns
        A binary matrix representation of the input. The classes axis
        is placed last.
    """
    y = np.array(y, dtype='int')
    input_shape = y.shape
    if input_shape and input_shape[-1] == 1 and len(input_shape) > 1:
        input_shape = tuple(input_shape[:-1])
    y = y.ravel()
    if not num_classes:
        num_classes = np.max(y) + 1
    n = y.shape[0]
    categorical = np.zeros((n, num_classes), dtype=dtype)
    categorical[np.arange(n), y] = 1
    output_shape = input_shape + (num_classes,)
    categorical = np.reshape(categorical, output_shape)
    return torch.from_numpy(categorical)

def mask_replace_num(mask,originNum,newNum):
    masktmp = copy.deepcopy(mask)
    if originNum in mask:
        masktmp[np.where(mask == originNum)] = newNum
    return masktmp

def model4_to_10(mask):
    mask = mask_replace_num(mask,4,10)
    mask = mask_replace_num(mask,3,8)
    mask = mask_replace_num(mask, 2, 4)
    mask = mask_replace_num(mask, 1, 3)
    return mask

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-result_model1", type=str,
                        default=r"C:\Users\zengxh\Desktop\results-4fenlei",  # 集成4
                        help="传入image_n_predict所在路径")
    parser.add_argument("-result_model2", type=str,
                        default=r"C:\Users\zengxh\Desktop\results-3807", # 数据增强0-180
                        help="传入image_n_predict所在路径")
    parser.add_argument("-result_model3", type=str,
                        default=r"C:\Users\zengxh\Desktop\results-3879", # 数据增强0-180
                        help="传入image_n_predict所在路径")
    arg = parser.parse_args()
    result_model1 = arg.result_model1
    result_model2 = arg.result_model2
    result_model3 = arg.result_model3

    labelsjson = {
        1: "耕地",
        2: "林地",
        3: "草地",
        4: "道路",
        5: "城镇建设用地",
        6: "农村建设用地",
        7: "工业用地",
        8: "构筑物",
        9: "水域",
        10: "裸地"
    }

    for mask_name in tqdm(os.listdir(result_model1)):
        mask1 = np.asarray(Image.open(os.path.join(result_model1, mask_name)))
        mask2 = np.asarray(Image.open(os.path.join(result_model2, mask_name))) # 3，4，8，10
        mask3 = np.asarray(Image.open(os.path.join(result_model3, mask_name)))  # 3，4，8，10
        mask4 = np.asarray(Image.open(os.path.join(r"C:\Users\zengxh\Desktop\results-3967", mask_name)))  # 3，4，8，10
        mask5 = np.asarray(Image.open(os.path.join(r"C:\Users\zengxh\Desktop\results-3927", mask_name)))  # 3，4，8，10
        mask6 = np.asarray(Image.open(os.path.join(r"C:\Users\zengxh\Desktop\results-3693", mask_name)))  # 3，4，8，10

        mask1 = model4_to_10(mask1)

        predict_list = to_categorical(mask1, num_classes=len(labelsjson) +1) + to_categorical(mask2, num_classes=len(labelsjson) +1) + \
                       to_categorical(mask3, num_classes=len(labelsjson) +1) + \
                       to_categorical(mask4, num_classes=len(labelsjson) + 1) + \
                       to_categorical(mask5, num_classes=len(labelsjson) + 1) + \
                       to_categorical(mask6, num_classes=len(labelsjson) + 1)
        result = torch.argmax(predict_list.cpu(), -1).byte().numpy()  # 256*256
        x1, x2 = np.min(result), np.max(result)
        if x1 <= 0 or x2 >= 11:
            print("result.shape", result.shape, type(result), result, x1, x2)
        cv.imwrite(os.path.join(
            r"C:\Users\zengxh\Desktop\results",
            mask_name), result)

        # mask_post = label_resize_vis(label, source_image)
        # cv.imwrite(os.path.join(r"C:\Users\zengxh\Desktop\viresult",mask_name), mask_post)
