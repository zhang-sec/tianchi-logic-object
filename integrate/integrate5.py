'''
Author      : now more
Connect     : lin.honghui@qq.com
LastEditors: Please set LastEditors
Description :
LastEditTime: 2020-11-27 03:42:46
'''
import copy
import os
import threading
import cv2 as cv
import numpy as np
from skimage.morphology import remove_small_holes, remove_small_objects
from argparse import ArgumentParser
from PIL import Image
import matplotlib.pyplot as plt
from tqdm import tqdm

def use_mask2(mask1,mask2,classnum):
    mask1tmp = copy.deepcopy(mask1)
    if classnum in mask2:
        mask1tmp[np.where(mask2 == classnum)] = classnum
    return mask1tmp

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-result_model1", type=str,
                        default=r"C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\data\integrate4-result",  # 集成4
                        help="传入image_n_predict所在路径")
    parser.add_argument("-result_model2", type=str,
                        default=r"C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\data\integrate5-model2", # 数据增强0-180
                        help="传入image_n_predict所在路径")
    arg = parser.parse_args()
    result_model1 = arg.result_model1
    result_model2 = arg.result_model2

    labelsjson = {
        1: "耕地",
        2: "林地",
        3: "草地",
        4: "道路",
        5: "城镇建设用地",
        6: "农村建设用地",
        7: "工业用地",
        8: "构筑物",
        9: "水域",
        10: "裸地"
    }

    for mask_name in tqdm(os.listdir(result_model1)):
        mask1 = np.asarray(Image.open(os.path.join(result_model1, mask_name)))
        mask2 = np.asarray(Image.open(os.path.join(result_model2, mask_name))) # 3，4，8，10

        # 以下4类不处理
        mask1 = use_mask2(mask1,mask2,5)
        mask1 = use_mask2(mask1,mask2,6)
        mask1 = use_mask2(mask1,mask2,7)
        mask1 = use_mask2(mask1,mask2,9)
        cv.imwrite(os.path.join(
            r"C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\data\integrate5-result",
            mask_name), mask1)

        # mask_post = label_resize_vis(label, source_image)
        # cv.imwrite(os.path.join(r"C:\Users\zengxh\Desktop\viresult",mask_name), mask_post)
