import os
import os.path as osp
import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import PIL.Image as Image
from collections import Counter
from tqdm import tqdm


def get_label_number(mask_dataset):

    label_count = Counter()

    for mask_path in tqdm(mask_dataset):
        mask_img = Image.open(mask_path)
        label_count.update(np.array(mask_img).flatten())

    return label_count
train_dataset_path = r"/home/admin/tianchi/tianchi-logic-object/data/tc/train_labels/"
train_mask = glob.glob(osp.join(train_dataset_path, '*.png'))
label_count = get_label_number(train_mask)

print(label_count)
# weights=[(173067508),
#        (783364798),
#        (6086055),
#        (17751239),
#        (5458338),
#        (19350932),
#        (4600435),
#        (3419876),
#        (35714475),
#        (876456)]
#
# weights2=[(173067508/1049690112),
#        (783364798/1049690112),
#        (6086055/1049690112),
#        (17751239/1049690112),
#        (5458338/1049690112),
#        (19350932/1049690112),
#        (4600435/1049690112),
#        (3419876/1049690112),
#        (35714475/1049690112),
#        (876456/1049690112)]
# print(weights)
# print(weights2)
# weights = np.array(weights)
# weights = 1 / weights  # number of targets per class
# weights /= weights.sum()  # normalize
# print(weights)