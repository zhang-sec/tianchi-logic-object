#!/usr/bin/env python

import cv2
import numpy as np
import copy
from test_transforms import draw, mask_vis


def mask_replace_num(mask,originNum,newNum):
    masktmp = copy.deepcopy(mask)
    if originNum in mask:
        masktmp[np.where(mask == originNum)] = newNum
    return masktmp

class Segment:
    def __init__(self, segments,bestLabels):
        # define number of segments, with default 5
        self.segments = segments
        self.bestLabels = bestLabels

    def kmeans(self, image):
        image = cv2.GaussianBlur(image, (7, 7), 0)
        vectorized = image.reshape(-1, 3)
        vectorized = np.float32(vectorized)
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        reshaped_labels = self.bestLabels.reshape(-1,1)
        reshaped_labels=reshaped_labels.astype(np.int32)
        ret, label, center = cv2.kmeans(vectorized, self.segments, reshaped_labels, criteria, 10, cv2.KMEANS_USE_INITIAL_LABELS)
                                        #data, K, bestLabels, criteria, attempts, flags, centers=None
        res = center[label.flatten()]
        segmented_image = res.reshape((image.shape))
        return label.reshape((image.shape[0], image.shape[1])), segmented_image.astype(np.uint8)

    def extractComponent(self, image, label_image, label):
        component = np.zeros(image.shape, np.uint8)
        component[label_image == label] = image[label_image == label]
        return component


if __name__ == "__main__":
    import argparse
    import sys
    image = cv2.imread(r"C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\data\suichang_round1_test_partA_210120\000009.tif")
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    mask = cv2.imread(r"C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\data\3879\000009.png", cv2.IMREAD_GRAYSCALE)



    #for i in range(len(set(mask.flatten().tolist()))):
    mask = mask_replace_num(mask,1,0)
    mask = mask_replace_num(mask,2,1)
    mask = mask_replace_num(mask,4,2)
    mask = mask_replace_num(mask,9,3)

    image=mask_vis(mask, image,alpha=0.1)

    cv2.imshow("image", image)
    draw(image, mask)



    seg = Segment(len(set(mask.flatten().tolist())),mask)
    label, result = seg.kmeans(image)
    cv2.imshow("segmented", result)

    label = mask_replace_num(label,3,9)
    label = mask_replace_num(label, 2, 4)
    label = mask_replace_num(label, 1, 2)
    label = mask_replace_num(label,0,1)

    draw(image,label)

    image = cv2.imread(r"C:\Users\zengxh\Documents\workspace\PyCharm-workspace\tianchi\tianchi-logic-object\data\suichang_round1_test_partA_210120\000009.tif")
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    mask = mask_replace_num(mask,3,9)
    mask = mask_replace_num(mask, 2, 4)
    mask = mask_replace_num(mask, 1, 2)
    mask = mask_replace_num(mask,0,1)



    draw(image,mask)
    cv2.waitKey(0)
