import ttach as tta
import torch
import torch.nn as nn
import segmentation_models_pytorch as smp
import albumentations as A
from albumentations.pytorch import ToTensorV2

import cv2
from PIL import Image
import numpy as np

import os
import glob
from tqdm import tqdm

class seg_qyl(nn.Module):
    def __init__(self, model_name, n_class):
        super().__init__()
        self.model = smp.UnetPlusPlus(
                encoder_name=model_name,        # choose encoder, e.g. mobilenet_v2 or efficientnet-b7
                encoder_weights='imagenet',     # use `imagenet` pretrained weights for encoder initialization
                in_channels=3,                  # model input channels (1 for grayscale images, 3 for RGB, etc.)
                classes=n_class,                      # model output channels (number of classes in your dataset)
            )

    def forward(self, x):
        x = self.model(x)
        return x

def get_infer_transform():
    transform = A.Compose([
        A.Resize(256, 256),
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
        ToTensorV2(),
    ])
    return transform

def inference(img_dir):
    transform=get_infer_transform()
    image = cv2.imread(img_dir, cv2.IMREAD_COLOR)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    img = transform(image=image)['image']
    img=img.unsqueeze(0)

    with torch.no_grad():
        img=img.cuda()
        output= tta_model(img)

    pred = output.squeeze().cpu().data.numpy()
    pred = np.argmax(pred,axis=0)
    return pred+1
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
if __name__ == "__main__":
    model_name = 'efficientnet-b7'  # 'efficientnet-b6'
    n_class = 10
    model = seg_qyl(model_name, n_class).cuda()
    model = torch.nn.DataParallel(model)
    checkpoints = torch.load('./outputs/39.43lovaszefficientnet-b7/ckpt/checkpoint-best.pth')
    model.load_state_dict(checkpoints['state_dict'])

    tta_transforms = tta.Compose(
        [
            tta.Scale(scales=[1.5]),
            tta.HorizontalFlip(),
            tta.VerticalFlip()
        ]
    )
    # D4 makes horizontal and vertical flips + rotations for [0, 90, 180, 270] angels.
    # and then merges the result masks with merge_mode="mean"
    tta_model = tta.SegmentationTTAWrapper(model,tta_transforms, merge_mode="mean")
    tta_model.eval()

    out_dir = 'results/'
    if not os.path.exists(out_dir): os.makedirs(out_dir)
    test_paths = glob.glob('/dat01/liuweixing/tianchi/test/*')
    # test_paths = glob.glob('D:\\dl\\tianchi\\test\\*')
    for per_path in tqdm(test_paths):
        result = inference(per_path)
        img = Image.fromarray(np.uint8(result))
        img = img.convert('L')

        out_path = os.path.join(out_dir, per_path.split('/')[-1][:-4] + '.png')
        # out_path = os.path.join(out_dir, per_path.split('\\')[-1][:-4] + '.png')
        img.save(out_path)

