cp code/resnet_3693.py code/mmsegmentation/mmseg/models/backbones/resnet.py
cd code/mmsegmentation
python tools/test.py configs/TianchiSeg/baseline3693.py ../../user_data/model_data/iter_80000_3693.pth
mv ../../user_data/model_data/results/ ../../user_data/tmp_data/results3693

cd ../../
cp code/resnet_3927.py code/mmsegmentation/mmseg/models/backbones/resnet.py
cd code/mmsegmentation
python tools/test.py configs/TianchiSeg/baseline3927.py ../../user_data/model_data/iter_120000_3927.pth
mv ../../user_data/model_data/results/ ../../user_data/tmp_data/results3927

cd ../../
cd code/net/
python tif_jpg.py
python make_dataset.py
python infer.py

cd ..
python integrate-finally-model.py

python code/cvkmeans3.py