# ~/.profile: executed by Bourne-compatible login shells.

if [ "$BASH" ]; then
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc
  fi
fi

mesg n || true

export CUDA_HOME="/usr/local/cuda-10.2"
export TENSORRT_HOME="/TensorRT-7.2.2.3"
export LD_LIBRARY_PATH="$TENSORRT_HOME/lib:$CUDA_HOME/lib:$CUDA_HOME/lib64:$LD_LIBRARY_PATH"
export PATH="$PATH:$CUDA_HOME/bin:$TENSORRT_HOME"