import base64
import glob
import json
import time
from logging import log
import numpy as np
import cv2
import requests
from future.backports.http.client import REQUEST_TIMEOUT
from io import BytesIO
from PIL import Image
import os

from tqdm import tqdm

from ours_code.net1.utils import IOUMetric


def loader2json(data):
    # send_json = json.loads(base_json, encoding='utf-8')
    send_json={}
    bast64_data = base64.b64encode(data)
    bast64_str = str(bast64_data,'utf-8')
    send_json['img'] = bast64_str
    send_json = json.dumps(send_json)
    return send_json


def analysis_res(res):
    # print(res)
    # print(res.content)
    print(res.status_code)
    print(res.text)
    return res


def send_eval(data, log):
    url = "http://127.0.0.1:8080/tccapi"
    start = time.time()
    res = requests.post(url, data, timeout=REQUEST_TIMEOUT)
    cost_time = time.time() - start
    # res = analysis_res(res)
    return res, cost_time

def result_to_nparray(ret):
    # 将response data转为numpy
    bast64_data = ret.text.encode(encoding='utf-8')
    img = base64.b64decode(bast64_data)
    bytesIO = BytesIO()
    img = Image.open(BytesIO(bytearray(img)))
    img = np.array(img)
    img = img.astype(np.uint8)

    # 将nparray 保存为图片
    # img = img.astype(np.float32)
    # img = Image.fromarray(img)
    # img = img.convert('L')
    # img = img.resize((256, 256), resample=Image.NEAREST)
    # img.save("/user_data/tmp_data/testcresults/000013.png")
    return img


val_img_dir = "/user_data/tmp_data/tc/val_images/"
val_make_dir = "/user_data/tmp_data/tc/val_labels/"

iou = IOUMetric(10)
xtimes = []
for val_origin_img_name in tqdm(os.listdir(val_img_dir)):
    # if val_origin_img_name == "000942.tif":
        img_path = os.path.join(val_img_dir, val_origin_img_name)

        make_path = os.path.join(val_make_dir, val_origin_img_name.replace(".tif", ".png"))
        mask = cv2.imread(make_path, cv2.IMREAD_GRAYSCALE)

        fin = open(img_path, 'rb')
        img = fin.read()
        data_json = loader2json(img)
        start = time.time()
        ret, cost_time = send_eval(data_json, log)
        xtimes.append(time.time() - start)

        result = result_to_nparray(ret)
        try:
            # print(np.max(result), np.min(result), np.max(mask), np.min(mask))
            iou.add_batch(result-1, mask-1)
        except Exception as e:
            print(str(e))

acc, acc_cls, iu, mean_iu, fwavacc=iou.evaluate()

xmax=800.0
xmin=40.0
Fe = 0.4 *(1-(min(max(sum(xtimes),xmin),xmax)-xmin)/(xmax-xmin))

final_score = 0.7*mean_iu+ 0.3*Fe
print("iu:{},\nmean_iu:{},Fe:{},final_score:{}".format(iu,mean_iu,Fe,final_score))
# infer.py mean_iu:0.5683662187272269,Fe:0.3619147872924805,final_score:0.506430789296803
# infer_onnx_tensorrt.py
# infer_tensorrt.py mean_iu:0.5681996561903728,Fe:0.3795189600241812,final_score:0.5115954473405153
# infer_net3_normal.py mean_iu:0.6624228399697701,Fe:0.13172158881237636,final_score:0.503212464622552
# infer_net3_tensorrt.py mean_iu:0.662396917052296,Fe:0.31781383225792337,final_score:0.5590219916139841
# infer_net3_tensorrt2.py mean_iu:0.6768621323000535,Fe:0.3587382153460854,final_score:0.5814249572138631
# infer_net3_tensorrt3.py mean_iu:0.676865952931714,Fe:0.36700803129296555,final_score:0.5839085764400894
# infer_net3_tensorrt3_tta.py mean_iu:0.6799720691333553,Fe:0.3662046621975146,final_score:0.585841847052603

