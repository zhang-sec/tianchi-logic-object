import sys
sys.path.insert(0, '/ours_code/hrnet/lib/')
import argparse
import base64
import glob
import json
import logging
import os
import pprint
import shutil
import sys
import time
import timeit

import albumentations as A
import cv2
import numpy as np
import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn
from ai_hub import inferServer
from albumentations.pytorch import ToTensorV2
from PIL import Image
from torch.nn import functional as F

from ours_code.hrnet.lib.models.seg_hrnet import get_seg_model
from ours_code.hrnet.lib.config import update_config, config


def parse_args():
    parser = argparse.ArgumentParser(description="Train segmentation network")

    parser.add_argument('--cfg',
                        help='experiment configure file name',
                        default="/ours_code/hrnet/experiments/tc/seg_hrnet_w18_256x256_sgd_lr7e-3_wd1e-4_bs_16_epoch300.yaml",
                        type=str)
    parser.add_argument("--ckpt_path", help="checkpoint path",
                        default="/ours_code/hrnet/output/tc/best.pth", type=str)
    parser.add_argument(
        "opts",
        help="Modify config options using the command-line",
        default=None,
        nargs=argparse.REMAINDER,
    )

    args = parser.parse_args()
    update_config(config, args)

    return args


class myInfer(inferServer):
    def __init__(self, model):
        super().__init__(model)

    def pre_process(self, data):
        json_data = json.loads(data.get_data().decode("utf-8"))
        img = json_data.get("img")
        bast64_data = img.encode(encoding="utf-8")
        img = base64.b64decode(bast64_data)
        img = cv2.imdecode(np.fromstring(img, dtype=np.uint8), -1)
        return img

    def post_process(self, data):
        result = np.array(data).astype(np.uint8)
        img = Image.fromarray(result)
        img = img.convert("L")
        processed_data = np.asarray(img)
        img_encode = np.array(cv2.imencode(".png", processed_data)[1]).tobytes()
        bast64_data = base64.b64encode(img_encode)
        bast64_str = str(bast64_data, "utf-8")
        return bast64_str


class mymodel(object):
    def __init__(self, ckpt_path):
        model = get_seg_model(config)
        model_dict = model.state_dict()
        pretrained_dict = torch.load(ckpt_path)
        pretrained_dict = {
            k[6:]: v for k, v in pretrained_dict.items() if k[6:] in model_dict.keys()
        }
        model_dict.update(pretrained_dict)
        model.load_state_dict(pretrained_dict)
        model = model.cuda()
        model.eval()

        self.model = model
        self.transform = A.Compose(
            [
                A.Normalize(
                    mean=(0.485, 0.456, 0.406, 0.5), std=(0.229, 0.224, 0.225, 0.25)
                ),
                ToTensorV2(),
            ]
        )

    def inference(self, image):
        img = self.transform(image=image)["image"]
        img = img.unsqueeze(0)
        with torch.no_grad():
            img = img.cuda()
            output = self.model(img)
            pred = F.interpolate(
                input=output,
                size=(config.TRAIN.IMAGE_SIZE[1], config.TRAIN.IMAGE_SIZE[0]),
                mode="bilinear",
                align_corners=False,
            )
        pred = pred[0].squeeze().cpu().data.numpy()
        pred = np.argmax(pred, axis=0)
        return pred + 1

    def __call__(self, image):
        return self.inference(image)


if __name__ == "__main__":
    args = parse_args()
    mymodel = mymodel(args.ckpt_path)
    my_infer = myInfer(mymodel)
    my_infer.run(ip="0.0.0.0",debuge=False)
