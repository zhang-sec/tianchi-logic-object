import torchvision
import base64
import albumentations
import cv2
import numpy as np
import torch

def print_version():
    print("torch version", torch.__version__)
    print("torchvision version", torchvision.__version__)
    print("albumentations version", albumentations.__version__)
    print("cv2 version", cv2.__version__)
    print("np version", np.__version__)

if __name__ == "__main__":
    img_path = "/user_data/tmp_data/tc/val_images/1_015401.tif"
    fin = open(img_path, 'rb')
    img = fin.read()
    bast64_data = base64.b64encode(img)
    bast64_str = str(bast64_data,'utf-8')
    img = bast64_str
    bast64_data = img.encode(encoding="utf-8")
    img = base64.b64decode(bast64_data)
    img = cv2.imdecode(np.fromstring(img, dtype=np.uint8), -1)

    transform1 = albumentations.Compose(
        [
            albumentations.Normalize(
                mean=(0.485, 0.456, 0.406, 0.5), std=(0.229, 0.224, 0.225, 0.25)
            )
        ]
    )
    output1 = transform1(image=img)["image"]
    output1 = output1.transpose((2, 0, 1))

    torch.set_printoptions(precision=9)
    input2 = torch.tensor(img)
    input2 = input2.float()
    input2 = input2.permute(2, 0, 1)
    dtype = input2.dtype
    mean = [0.485, 0.456, 0.406, 0.5]
    std =[0.229, 0.224, 0.225, 0.25]
    mean = torch.tensor(mean,dtype=torch.float32)
    mean.mul_(255.0)
    std = torch.tensor(std,dtype=torch.float32)
    std.mul_(255.0)
    std = torch.reciprocal(std)
    mean = torch.as_tensor(mean, dtype=dtype, device=input2.device)
    std = torch.as_tensor(std, dtype=dtype, device=input2.device)
    if mean.ndim == 1:
        mean = mean.view(-1, 1, 1)
    if std.ndim == 1:
        std = std.view(-1, 1, 1)
    input2.sub_(mean).mul_(std)

    print("sdf")


