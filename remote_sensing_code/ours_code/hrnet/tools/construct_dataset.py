import copy
import glob
import os
import random
import shutil

import cv2
import numpy as np
from tqdm import tqdm
from PIL import Image

random.seed(2021)
val_ratio = 0.2
base_dir = "/user_data/tmp_data/tc"
train_dir_part1 = "/tcdata/suichang_round1_train_210120"
train_dir_part2 = "/tcdata/suichang_round2_train_210316"
train_images_dir = os.path.join(base_dir, "train_images")
val_images_dir = os.path.join(base_dir, "val_images")
train_labels_dir = os.path.join(base_dir, "train_labels")
val_labels_dir = os.path.join(base_dir, "val_labels")
os.makedirs(train_images_dir, exist_ok=True)
os.makedirs(val_images_dir, exist_ok=True)
os.makedirs(train_labels_dir, exist_ok=True)
os.makedirs(val_labels_dir, exist_ok=True)


# train val
tif_images = glob.glob(os.path.join(train_dir_part1, "*.tif")) + glob.glob(
    os.path.join(train_dir_part2, "*.tif")
)
random.shuffle(tif_images)
length = len(tif_images)

train_size = int((1 - val_ratio) * length)
val_size = length - train_size

train_images = tif_images[:train_size]
val_images = tif_images[train_size:]

def mask_replace_num(mask,originNum,newNum):
    masktmp = copy.deepcopy(mask)
    if originNum in mask:
        masktmp[np.where(mask == originNum)] = newNum
    return masktmp
def get_split_img(tif):
    mask_path = tif.replace(".tif", ".png")
    gt_dir_mask = np.asarray(Image.open(mask_path))
    gt_dir_mask = mask_replace_num(gt_dir_mask, 1, 1)
    gt_dir_mask = mask_replace_num(gt_dir_mask, 2, 1)
    # gt_dir_mask = mask_replace_num(gt_dir_mask, 3, 2)
    # gt_dir_mask = mask_replace_num(gt_dir_mask, 4, 3)
    gt_dir_mask = mask_replace_num(gt_dir_mask, 5, 1)
    gt_dir_mask = mask_replace_num(gt_dir_mask, 6, 1)
    gt_dir_mask = mask_replace_num(gt_dir_mask, 7, 1)
    # gt_dir_mask = mask_replace_num(gt_dir_mask, 8, 4)
    gt_dir_mask = mask_replace_num(gt_dir_mask, 9, 1)
    # gt_dir_mask = mask_replace_num(gt_dir_mask, 10, 5)
    return gt_dir_mask

for i,tif in tqdm(enumerate(train_images)):
    tifname = os.path.basename(tif)
    shutil.copy(tif, os.path.join(train_images_dir,"{}_{}".format(i,tifname)))

    # gt_dir_mask=get_split_img(tif)
    # cv2.imwrite(os.path.join(train_labels_dir,"{}_{}".format(i,tifname.replace(".tif", ".png"))), gt_dir_mask)

    shutil.copy(tif.replace(".tif", ".png"), os.path.join(train_labels_dir,"{}_{}".format(i,tifname.replace(".tif", ".png"))))

for i,tif in tqdm(enumerate(val_images)):
    tifname = os.path.basename(tif)
    shutil.copy(tif, os.path.join(val_images_dir,"{}_{}".format(i,tifname)))

    # gt_dir_mask=get_split_img(tif)
    # cv2.imwrite(os.path.join(val_labels_dir,"{}_{}".format(i,tifname.replace(".tif", ".png"))), gt_dir_mask)
    shutil.copy(tif.replace(".tif", ".png"), os.path.join(val_labels_dir,"{}_{}".format(i,tifname.replace(".tif", ".png"))))


with open(os.path.join(base_dir, "trainList.txt"), "w") as f:
    train_images = os.listdir(train_images_dir)
    for image in train_images:
        label = os.path.join(
            base_dir, os.path.split(train_labels_dir)[-1], image.replace(".tif", ".png")
        )
        image = os.path.join(base_dir, os.path.split(train_images_dir)[-1], image)
        f.write(image + " " + label + "\n")

with open(os.path.join(base_dir, "valList.txt"), "w") as f:
    val_images = os.listdir(val_images_dir)
    for image in val_images:
        label = os.path.join(
            base_dir, os.path.split(val_labels_dir)[-1], image.replace(".tif", ".png")
        )
        image = os.path.join(base_dir, os.path.split(val_images_dir)[-1], image)
        f.write(image + " " + label + "\n")
