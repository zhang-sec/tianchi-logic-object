EasyDict==1.7
shapely
Cython
scipy
pandas
pyyaml
json_tricks
scikit-image
yacs>=0.1.5
tensorboardX>=1.6
tqdm
ninja
albumentations
torch==1.6.0
torchvision==0.7.0
ai-hub
flask
segmentation_models_pytorch
onnx
pycuda
ttach==0.0.3