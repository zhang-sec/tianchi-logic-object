import numpy as np
import cv2
import onnxruntime as rt



def load_and_run(file_path = "/user_data/model_data/checkpoint-best.onnx"):

    image = cv2.imread("/user_data/tmp_data/img_dir/val/0_000001.jpg", cv2.IMREAD_COLOR)
    image_cv = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image_cv = cv2.resize(image_cv, (256, 256))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    img_np = np.array(image_cv, dtype=float) / 255.
    img_np = (img_np - mean) / std
    img_np = img_np.transpose((2, 0, 1))
    img_np_nchw = np.expand_dims(img_np, 0).astype(np.float16)
    img_np_nchw = np.array(img_np_nchw, dtype=img_np_nchw.dtype, order='C')
    sess = rt.InferenceSession(file_path)
    outputs = ["binary_seg_ret:0", "instance_seg_ret:0"]
    result = sess.run(outputs, {"input_tensor:0": img_np_nchw})
    b, i= result
    print(b.shape)
    print(i.shape)

if __name__ == "__main__":
    load_and_run()