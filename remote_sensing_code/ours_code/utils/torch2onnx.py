import torch
import onnx
from ours_code.net1.models.model import seg_qyl

model_name = 'resnet50'  # 'efficientnet-b6'
n_class = 10
model = seg_qyl(model_name, n_class)
# net1 = torch.nn.DataParallel(net1) # 如果使用了多卡训练那么需要加这句话
checkpoints = torch.load('/user_data/model_data/checkpoint-best.pth')
model.load_state_dict(checkpoints['state_dict'])
model.cuda()
model.eval()

# for name, param in model.named_parameters():
#     param.half()

dummy_input = torch.randn(1, 3, 256, 256).cuda().half()
onnx_path = '/user_data/model_data/checkpoint-best.onnx'
torch.onnx.export(model, dummy_input, onnx_path, opset_version=11, verbose=True)

# 检查导出的model
onnx.checker.check_model(onnx.load(onnx_path))

