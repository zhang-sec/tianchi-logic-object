import os
from glob import glob

img_dir = '/home/trojanjet/yaogan_seg/suichang_unetpp/user_data/tmp_data2/img_dir'
ann_dir = '/home/trojanjet/yaogan_seg/suichang_unetpp/user_data/tmp_data2/ann_dir'

data_lst_txt_dir = '/home/trojanjet/yaogan_seg/HRNet-Semantic-Segmentation/data/list/suichang'
os.makedirs(data_lst_txt_dir, exist_ok=True)

with open(os.path.join(data_lst_txt_dir, 'trainList.txt'), 'w') as f:
    img_paths = glob(f'{img_dir}/train/*.jpg')
    for img_path in img_paths:
        label_path = os.path.join(ann_dir, 'train', os.path.basename(img_path).replace('.jpg', '.png'))
        f.write('{} {}\n'.format(img_path, label_path))

with open(os.path.join(data_lst_txt_dir, 'valList.txt'), 'w') as f:
    img_paths = glob(f'{img_dir}/val/*.jpg')
    for img_path in img_paths:
        label_path = os.path.join(ann_dir, 'val', os.path.basename(img_path).replace('.jpg', '.png'))
        f.write('{} {}\n'.format(img_path, label_path))

with open(os.path.join(data_lst_txt_dir, 'trainval.txt'), 'w') as f:
    img_paths = glob(f'{img_dir}/train_val/*.jpg')
    for img_path in img_paths:
        label_path = os.path.join(ann_dir, 'train_val', os.path.basename(img_path).replace('.jpg', '.png'))
        f.write('{} {}\n'.format(img_path, label_path))
