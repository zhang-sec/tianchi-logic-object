import albumentations as A
from albumentations.pytorch import ToTensorV2
import numpy as np
import torch

image_size = 256
max_size = int(image_size * 2.0) # 放大到最大的比例
middle_size = int(image_size * 1.25)#初始1.5 1.75 中间尺寸比例

train_transform = A.Compose([
    A.OneOf([
        A.Compose([
            A.LongestMaxSize(max_size, p=1),
            A.RandomCrop(image_size, image_size, p=1)
        ]),
        A.Compose([
            A.Resize(image_size, image_size, p=1)
        ]),
        A.Compose([
            A.LongestMaxSize(middle_size, p=1),
            A.RandomCrop(image_size, image_size, p=1)
        ]),
    ]),

    # A.RandomGridShuffle(),

    A.HueSaturationValue(),
    # A.RandomGamma(),
    # A.RandomBrightnessContrast(),

    # A.GaussNoise(),
    # A.GaussianBlur(),

    A.OneOf([
        A.VerticalFlip(p=0.5),
        A.HorizontalFlip(p=0.5),
        A.RandomRotate90(),
        A.Transpose(p=0.5)
    ]),
    A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
    ToTensorV2(),
])

val_transform = A.Compose([
    # A.Resize(max_size, max_size),
    A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
    ToTensorV2(),
])

def mixup_data(x, y, alpha=1.0, device="cpu"):
    '''Compute the mixup data. Return mixed inputs, pairs of targets, and lambda'''
    if alpha > 0.:
        lam = np.random.beta(alpha, alpha)
    else:
        lam = 1.
    batch_size = x.size()[0]
    if str(device) != "cpu":
        index = torch.randperm(batch_size).to(device)
    else:
        index = torch.randperm(batch_size)

    mixed_x = lam * x + (1 - lam) * x[index, :]
    y_a, y_b = y, y[index]
    return mixed_x, y_a, y_b, lam


def mixup_criterion(y_a, y_b, lam):
    return lambda criterion, pred: lam * criterion(pred, y_a) + (1 - lam) * criterion(pred, y_b)