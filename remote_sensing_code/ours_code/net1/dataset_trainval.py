import os
import shutil
from tqdm import tqdm

work_dir = '/dat01/liuweixing/tianchi'
alltarin_dir = work_dir + '/suichang_round1_train_210120'
val_dir = work_dir + '/val'

if not os.path.exists(val_dir):os.makedirs(val_dir)
val_ratio=0.2
val_interval=int((1/val_ratio))

val_size=0
names=[os.path.splitext(file)[0] for file in os.listdir(alltarin_dir) if file.endswith('.tif')]
train_size = len(names)
for i in tqdm(range(len(names))):
    if i%val_interval==0:
        name = names[i] + '.tif'
        mask_name = names[i] + '.png'
        shutil.copy(os.path.join(alltarin_dir,name),os.path.join(val_dir,name))
        shutil.copy(os.path.join(alltarin_dir,mask_name), os.path.join(val_dir, mask_name))
        val_size+=1

print("train size:{},val size:{}".format(train_size,val_size))