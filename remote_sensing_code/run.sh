/usr/sbin/service ssh restart
#. /root/.bashrc

# net1 start
/net1/bin/python train.py

## net1 train
#/net1/bin/python ./ours_code/net1/tif_jpg.py
#/net1/bin/python ./ours_code/net1/make_dataset.py
#/net1/bin/python ./ours_code/net1/train.py
#/net1/bin/python ./ours_code/net1/infer.py
## net1 end
#
#/net1/bin/python infer.py

#net3 start
#/opt/conda/envs/net3/bin/python3.8 /ours_code/hrnet/tools/construct_dataset.py
#/opt/conda/envs/net3/bin/python3.8 /ours_code/hrnet/tools/train.py
#/opt/conda/envs/net3/bin/python3.8 -m torch.distributed.launch --nproc_per_node=2 /ours_code/hrnet/tools/train2.py # 二阶段训练
#cp /ours_code/hrnet/output/tc/seg_hrnet_w18_256x256_sgd_lr7e-3_wd1e-4_bs_16_epoch300/best.pth /ours_code/hrnet/output/tc/best.pth
#
#/opt/conda/envs/net3/bin/python3.8 /ours_code/hrnet/tensorrt/torch2onnx3.py
#/TensorRT-7.2.2.3/bin/trtexec --onnx=/user_data/model_data/checkpoint-best.onnx --tacticSources=-cublasLt,+cublas --workspace=2048 --fp16 --saveEngine=/user_data/model_data/checkpoint-best.engine

/opt/conda/envs/net3/bin/python3.8 infer_net3_tensorrt3.py
#net3 end