import os
import cv2
import numpy as np
import paddlex as pdx
from tqdm import tqdm
import skimage.io as skimg_io

### 这里需改为测试数据的目录
TEST_DIR = 'data/suichang_round1_test_partA_210120'
OUTPUT_PATH = 'work/output/HRNet64/results'
VISUALIZE_PATH = 'work/output/HRNet64/visualized'
model = pdx.load_model('Models/HRNet64/best_model/')
print('Load Model.')

img_vis = np.zeros([256, 256, 3], dtype=np.uint8)
for f in tqdm(os.listdir(TEST_DIR)):
    path = os.path.join(TEST_DIR, f)
    img = skimg_io.imread(path)
    results = model.predict(img)
    pdx.seg.visualize(img_vis, results, weight=0.0, save_dir=VISUALIZE_PATH)
    label = results['label_map']
    score_map = results['score_map']
    out_pt = os.path.join(OUTPUT_PATH, f.replace('.tif', '.png'))
    # 做了一次标签转换
    cv2.imwrite(out_pt, label + 1)

os.system('cd ~/work/output/HRNet64 && zip -qr results.zip results')
os.system('cd ~/work/output/HRNet64 && zip -qr visualized.zip visualized')