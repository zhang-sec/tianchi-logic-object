# coding: utf8
# copyright (c) 2020 PaddlePaddle Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# from .ops import *
# from .imgaug_support import execute_imgaug
# import random
import os.path as osp
import numpy as np
from PIL import Image
import cv2
import imghdr
# import six
# import sys
# from collections import OrderedDict

import paddlex.utils.logging as logging


class SegTransform:
    """ 分割transform基类
    """

    def __init__(self):
        pass


class Compose(SegTransform):
    """根据数据预处理/增强算子对输入数据进行操作。
       所有操作的输入图像流形状均是[H, W, C]，其中H为图像高，W为图像宽，C为图像通道数。
    Args:
        transforms (list): 数据预处理/增强算子。
    Raises:
        TypeError: transforms不是list对象
        ValueError: transforms元素个数小于1。
    """

    def __init__(self, transforms):
        if not isinstance(transforms, list):
            raise TypeError('The transforms must be a list!')
        if len(transforms) < 1:
            raise ValueError('The length of transforms ' + \
                            'must be equal or larger than 1!')
        self.transforms = transforms
        self.batch_transforms = None
        self.to_rgb = False
        # 检查transforms里面的操作，目前支持PaddleX定义的或者是imgaug操作
        # for op in self.transforms:
        #     if not isinstance(op, SegTransform):
        #         import imgaug.augmenters as iaa
        #         if not isinstance(op, iaa.Augmenter):
        #             raise Exception(
        #                 "Elements in transforms should be defined in 'paddlex.seg.transforms' or class of imgaug.augmenters.Augmenter, see docs here: https://paddlex.readthedocs.io/zh_CN/latest/apis/transforms/"
        #             )

    @staticmethod
    def read_img(img_path, input_channel=3):
        img_format = imghdr.what(img_path)
        name, ext = osp.splitext(img_path)
        if img_format == 'tiff' or ext == '.img':
            import skimage.io as skimg_io
            dataset = skimg_io.imread(img_path)
            if dataset is None:
                raise Exception('Can not open', img_path)
            im_data = dataset
            return im_data
        else:
            return cv2.imread(img_path)
        # elif img_format in ['png']:
        #     if input_channel == 3:
        #         return cv2.imread(img_path)
        #     else:
        #         return cv2.imread(img_path, cv2.IMREAD_UNCHANGED)
        # elif img_format in ['jpeg', 'bmp']:
        #     if input_channel == 3:
        #         return cv2.imread(img_path)
        #     else:
        #         return cv2.imread(img_path, cv2.IMREAD_UNCHANGED)
        # elif ext == '.npy':
        #     return np.load(img_path)
        # else:
        #     raise Exception('Image format {} is not supported!'.format(ext))

    @staticmethod
    def decode_image(im_path, label, input_channel=3):
        if isinstance(im_path, np.ndarray):
            if len(im_path.shape) != 3:
                raise Exception(
                    "im should be 3-dimensions, but now is {}-dimensions".
                    format(len(im_path.shape)))
            im = im_path
        else:
            try:
                im = Compose.read_img(im_path, input_channel).astype('float32')
            except:
                raise ValueError('Can\'t read The image file {}!'.format(
                    im_path))
        im = im.astype('float32')
        if label is not None:
            if isinstance(label, np.ndarray):
                if len(label.shape) != 2:
                    raise Exception(
                        "label should be 2-dimensions, but now is {}-dimensions".
                        format(len(label.shape)))

            else:
                try:
                    ### 仅用于生态资产智能分析比赛，标签值减1
                    label = np.asarray(Image.open(label))-1
                except:
                    ValueError('Can\'t read The label file {}!'.format(label))
            im_height, im_width, _ = im.shape
            label_height, label_width = label.shape
            if im_height != label_height or im_width != label_width:
                raise Exception(
                    "The height or width of the image is not same as the label")
        return (im, label)

    def __call__(self, im, im_info=None, label=None):
        """
        Args:
            im (str/np.ndarray): 图像路径/图像np.ndarray数据。
            im_info (list): 存储图像reisze或padding前的shape信息，如
                [('resize', [200, 300]), ('padding', [400, 600])]表示
                图像在过resize前shape为(200, 300)， 过padding前shape为
                (400, 600)
            label (str/np.ndarray): 标注图像路径/标注图像np.ndarray数据。
        Returns:
            tuple: 根据网络所需字段所组成的tuple；字段由transforms中的最后一个数据预处理操作决定。
        """

        input_channel = getattr(self, 'input_channel', 3)
        im, label = self.decode_image(im, label, input_channel)
        if self.to_rgb and input_channel == 3:
            im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        if im_info is None:
            im_info = [('origin_shape', im.shape[0:2])]
        if label is not None:
            origin_label = label.copy()
        for op in self.transforms:
            # if isinstance(op, SegTransform):
            outputs = op(im, im_info, label)
            im = outputs[0]
            if len(outputs) >= 2:
                im_info = outputs[1]
            if len(outputs) == 3:
                label = outputs[2]
        if self.transforms[-1].__class__.__name__ == 'ArrangeSegmenter':
            if self.transforms[-1].mode == 'eval':
                if label is not None:
                    outputs = (im, im_info, origin_label)
        return outputs

    def add_augmenters(self, augmenters):
        if not isinstance(augmenters, list):
            raise Exception(
                "augmenters should be list type in func add_augmenters()")
        transform_names = [type(x).__name__ for x in self.transforms]
        for aug in augmenters:
            if type(aug).__name__ in transform_names:
                logging.error(
                    "{} is already in ComposedTransforms, need to remove it from add_augmenters().".
                    format(type(aug).__name__))
        self.transforms = augmenters + self.transforms


