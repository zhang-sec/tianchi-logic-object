import glob

import torch
import torch.nn as nn
from tqdm import tqdm

def process(param):
    new_pre = {}
    for k,v in param.items():
        name = k[7:]
        # new_pre[name]=v
        new_pre[k] = v
    return new_pre

def swa_offline(weight_path_list):
    merge_weight = {}
    net_weight_list = []
    num = len(weight_path_list)
    for i in tqdm(range(num)):
        # 如果是多卡训练的，这里需要调用process函数先处理下权重，单卡的不需要
        net_weight_list.append(process(torch.load(weight_path_list[i])['state_dict']))
    for key in net_weight_list[0].keys():
        tmp_list = [per[key] for per in net_weight_list]
        tmp = torch.true_divide(tmp_list[0], num)  # tmp_list[0]/num
        for j in range(1, num):
            tmp += torch.true_divide(tmp_list[j],num)  # tmp_list[j]/num
        merge_weight[key] = tmp
    return merge_weight

if __name__ == '__main__':
    path_1='/home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/iter_120000.pth'
    path_2='/home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/iter_110000.pth'
    path_3='/home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/iter_100000.pth'
    path_4='/home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/iter_90000.pth'
    path_5='/home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/iter_80000.pth'
    path_6='/home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline/iter_70000.pth'

    # model_list=glob.glob('/home/admin/tianchi/tianchi-logic-object/mmsegmentation/work_dirs/TianchiSeg/baseline_swa/iter_*.pth')
    model_list = [path_1,path_2,path_3,path_4,path_5,path_6]
    print(model_list)

    merge_weight=swa_offline(model_list)
    torch.save(merge_weight,'swa_b6.pth')
    #model.load_state_dict(merge_weight)
    print("finished ..........")