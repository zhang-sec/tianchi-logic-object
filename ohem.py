





def focal_loss(self, output, target, alpha, gamma, OHEM_percent):
	output = output.contiguous().view(-1)
	target = target.contiguous().view(-1)

	max_val = (-output).clamp(min=0)
	loss = output - output * target + max_val + ((-max_val).exp() + (-output - max_val).exp()).log()

	invprobs = F.logsigmoid(-output * (target * 2 - 1))
	focal_loss = alpha * (invprobs * gamma).exp() * loss

	OHEM, _ = focal_loss.topk(k=int(OHEM_percent * [*focal_loss.shape][0]))
	return OHEM.mean()